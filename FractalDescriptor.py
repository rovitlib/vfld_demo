import numpy as np
import open3d as o3d
import copy
import pickle
import random
import math
import matplotlib.pyplot as plt
from multiprocessing import Pool
import time



class FractalFeature(o3d.pipelines.registration.Feature):

	def __init__(self, features):
		super().__init__()
		features = np.array(features, dtype=np.float64)
		self.data = features.T
	

	def dimension(self):
		return self.data.shape[0]


	def num(self):
		return self.data.shape[1]


	def resize(self, dim, n):
		print("Not implemented yet")
		None



class FractalDescriptorExtractor:

	def __init__(self):
		None


	def get_neighbors_in_radius(pcd, pcdTree, radius, point):

		[_, idxs, _] = pcdTree.search_radius_vector_3d(point, radius)

		points = []
		for idx in idxs[1:]:
			points.append(pcd.points[idx])

		n_pcd = o3d.geometry.PointCloud()
		n_pcd.points = o3d.utility.Vector3dVector(points)

		return n_pcd


	def compute_fractal_dim(neighbors, boxCountIter, bc_subdivs):
		pcd = copy.copy(neighbors)

		minBound = pcd.get_min_bound()
		maxBound = pcd.get_max_bound()

		diffs = maxBound - minBound
		grid_size = np.max(diffs) 

		if grid_size==0: 
			return 0

		boxCountings = []
		leaf_sizes = []
		leaf_size = grid_size
		for i in range(boxCountIter):
			vxg = o3d.geometry.VoxelGrid.create_from_point_cloud_within_bounds(pcd,leaf_size,minBound,maxBound)
			occupied_voxels=len(vxg.get_voxels()) 

			boxCountings.append(occupied_voxels)
			leaf_sizes.append(leaf_size)
			leaf_size /= float(bc_subdivs)


		x_coords = np.log(np.divide(1,leaf_sizes))
		y_coords = np.log(boxCountings)

		line_fit = np.polyfit(x_coords,y_coords,1)
		slope = line_fit[0]


		return slope


	def compute_fractal_descriptor(self, pcd, radiusFactors, boxCountIter, bc_subdivs):

		distancias = pcd.compute_nearest_neighbor_distance()
		distanciaMedia = np.mean(np.array(distancias))

		radioCalculado = [x * distanciaMedia for x in radiusFactors]
		
		pcd_descriptors = [[] for _ in pcd.points]

		pcdTree = o3d.geometry.KDTreeFlann(pcd)
		

		def get_descriptor_of_a_point(point):
			fractalDimensionList = []
			for radius in radioCalculado:
				neighbors = FractalDescriptorExtractor.get_neighbors_in_radius(pcd, pcdTree, radius, point)
				fd = FractalDescriptorExtractor.compute_fractal_dim(neighbors, boxCountIter, bc_subdivs)
				fractalDimensionList.append(fd)
			return fractalDimensionList

		start = time.time()

		for i in range(len(pcd.points)):
			point = pcd.points[i]
			fractalDimensionList =  get_descriptor_of_a_point(point)
			pcd_descriptors[i] = fractalDimensionList

		end = time.time()

		return pcd_descriptors


