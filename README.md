# VFLD: Voxelized Fractal Local Descriptor #

Repository for the demo code of the Local Voxelized Fractal Descriptor.

## Requirements ##
- Open3D 0.13

`pip install open3d==0.13`

## Run the demo code ##

Two different scripts are provided:

`demo_acc_pr.py` extracts the VFLD features of two point clouds and performs nearest neighbor matching. It computes the accuracy and the precision recall curves. You can choose to run the demo on a single random cloud from the Simple Figures dataset or ModelNet10 subset dataset by modifying the `figure_source` variable.

`python demo_acc_pr.py`

`demo_reg.py` reads a sequence of points clouds and runs the registration pipeline. You can choose different scenes by modifying the path in line 73 of the script. The available scenes are in the `data` folder.

`python demo_reg.py`

## Results of applying VFLD to a regisration pipeline (Kinect v1 clouds) ##

![Reg1](figures/d4.png) ![Reg1](https://media.giphy.com/media/27Qt0QS1fZGYoQXxUH/source.gif) 

![Reg1](figures/d3.png) ![Reg1](https://media.giphy.com/media/JFI0a5tE41aiJsdsAx/source.gif) 

![Reg1](figures/d5.png) ![Reg1](https://media.giphy.com/media/5Eh2tCkLhVIqSBxQhy/source.gif)

![Reg1](figures/d1.png) ![Reg1](https://media.giphy.com/media/bL8cbVv85JdzmJCHJS/source.gif)

![Reg1](figures/d6.png) ![Reg1](https://media.giphy.com/media/maGNSmPiPDnmoX6AZi/source.gif)

![Reg1](figures/d7.png) ![Reg1](https://media.giphy.com/media/Bv0XToIEkG12VhHzvF/source.gif)

![Reg1](figures/d8.png) ![Reg1](https://media.giphy.com/media/JMnWXIfmxjtJBGIvmZ/source.gif)

![Reg1](figures/d9.png) ![Reg1](https://media.giphy.com/media/Tl99AL6G9TxCI4N8e3/source.gif)

![Reg1](figures/d10.png) ![Reg1](https://media.giphy.com/media/UOwrwJkm48UIaww0yj/source.gif)

![Reg1](figures/d11.png) ![Reg1](https://media.giphy.com/media/sajtTKEY9sjlpF1FGV/source.gif)


### Results on lidar data

![Reg1](https://media.giphy.com/media/smVoqv68cYF6P6hZKr/source.gif)

![Reg1](https://media.giphy.com/media/ijarzcF5RQw6T7l2Xz/source.gif)