import open3d as o3d
import numpy as np
import copy
import matplotlib.pyplot as plt
from tqdm import tqdm
import random
from FractalDescriptor import *
from utils import *





def compute_recall_precision(true_matches, false_matches, correspondences):

	recall = []
	precision = []
	for j in range(len(true_matches)):
		matches = true_matches[j]+false_matches[j]
		if matches == 0: matches=0.0000000001

		p = true_matches[j]/matches

		r = true_matches[j]/correspondences
		
		recall.append(r)
		precision.append(p)

	
	return recall, precision



def compute_tp_fp_corrs(pcd1, pcd2, pcd1_feat, pcd2_feat, ths, T, R):

	pcd2_feat_tree = o3d.geometry.KDTreeFlann(pcd2_feat)

	distancias = pcd1.compute_nearest_neighbor_distance()
	cloud_res = np.mean(np.array(distancias))

	matches = [0]*len(ths)
	true_matches = [0]*len(ths)
	false_matches = [0]*len(ths)
	correspondences = 0
	ros = []

	for i in range(len(pcd1.points)):
		correspondences += 1
		p1 = pcd1.points[i]
		f1 = pcd1_feat.data.T[i]
		r = pcd2_feat_tree.search_knn_vector_xd(f1, 2)
		d1 = r[2][0]
		d2 = r[2][1]
		if d2 == 0: d2 = 0.00000000000001
		ro = d1/d2
		ros.append(ro)

		for j in range(len(ths)):
			th = ths[j]
			if ro <= th:
				matches[j] += 1
				pc = pcd2.points[r[1][0]]
				pc2nn = pcd2.points[r[1][1]]
				d_3d = np.linalg.norm(p1-pc)

				if d_3d < cloud_res:
					true_matches[j] += 1
				else:
					false_matches[j] += 1
				
				

	return true_matches, false_matches, correspondences






def get_statistics(simple_figures_pcd_1, radiusFactors, boxCountIter, bc_subdivs):

	pr_ths = [0.2, 0.4, 0.6, 0.75, 0.85, 0.9, 0.95, 1.0]

	# pick a random sample for the demo
	rand_idx = random.randint(0,len(simple_figures_pcd_1)-1)


	pcd_1 = simple_figures_pcd_1[rand_idx]
	pcd_1 = scale_pcd(pcd_1,100)
	pcd_2 = copy.deepcopy(pcd_1)

	pcd_rot_1, T, R, = apply_random_T(pcd_1)

	print("Showing Source and Target PC")
	o3d.visualization.draw_geometries([pcd_rot_1,pcd_2])


	# Fractal descriptor computation
	extr = FractalDescriptorExtractor()
	pcd1_feat = extr.compute_fractal_descriptor(pcd_rot_1, radiusFactors, boxCountIter, bc_subdivs)
	pcd2_feat = extr.compute_fractal_descriptor(pcd_2, radiusFactors, boxCountIter, bc_subdivs)
	pcd1_feat = FractalFeature(pcd1_feat)
	pcd2_feat = FractalFeature(pcd2_feat)


	true_matches_curr, false_matches_curr, correspondences_curr = compute_tp_fp_corrs(pcd_1, pcd_2, pcd1_feat, pcd2_feat, pr_ths, T, R)


	recall, precision_inv = compute_recall_precision(true_matches_curr, false_matches_curr, correspondences_curr)
	avg_accuracy = np.array(true_matches_curr)/float(correspondences_curr)

	print("Displaying Accuracy")
	plt.clf()
	plt.plot(pr_ths, avg_accuracy)
	plt.legend(['Fractal'])
	plt.xlabel('Threshold')
	plt.ylabel('Accuracy')
	plt.show()

	print("Displaying PRC")
	plt.clf()
	plt.plot(recall, precision_inv)
	plt.legend(['Fractal'])
	plt.xlabel('Recall')
	plt.ylabel('Precision')
	plt.show()


	return recall, precision_inv







def main():

	figure_source = 1 # 0- synthetic, generated figures, 1- 10 first samples of each class modelnet


	number_of_points = 3000
	simple_figures_pcd = []
	if figure_source == 0:
		models_pcd = generate_simple_figures(number_of_points)
	elif figure_source == 1:
		models_pcd = generate_modelnet_figures(number_of_points, "data/ModelNet10_subset")


	print("Number of figures:",len(models_pcd))
	print("Figure Source:", end =" ")

	s = 1
	i = 0.5
	rl = 18
	radiusFactors = np.arange(s, 999.0, i)[:rl]
	boxCountIter = 5
	bc_subdivs = 2
	radiusFactors_m = np.array(radiusFactors)
	print("Radius Factors", radiusFactors_m)
	recall, precision_inv = get_statistics( models_pcd, 
											radiusFactors,
											boxCountIter,
											bc_subdivs
											)


	

if __name__ == '__main__':
	main()