import open3d as o3d
import time
import numpy as np
import math
import random
import copy

import matplotlib.pyplot as plt
from FractalDescriptor import *
from utils import *
import pickle as pkl
import os
from glob import glob
import sys
import re


def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def generate_simple_figures(number_of_points=500):

	meshes = []

	mesh = o3d.geometry.TriangleMesh.create_sphere()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_arrow()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_box()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_cone()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_cylinder()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_icosahedron()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_moebius()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_octahedron()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_tetrahedron()
	meshes.append(mesh)
	mesh = o3d.geometry.TriangleMesh.create_torus()
	meshes.append(mesh)

	pcds_1 = []
	for mesh in meshes:
		pcd_1 = mesh.sample_points_uniformly(number_of_points=number_of_points)
		
		pcd_1 = distort_x_axis(pcd_1, factor=1.5)
		pcd_1 = nornalize_unit_ball(pcd_1)
		pcds_1.append(pcd_1)


	return pcds_1


def generate_modelnet_figures(number_of_points, pathToDataset):
	
	off_files = [y for x in os.walk(pathToDataset) for y in glob(os.path.join(x[0], '*.off'))]
	random.shuffle(off_files)
	pcds_1 = []
	for model in off_files:
		mesh1 = o3d.io.read_triangle_mesh(model)
		pcd1 = o3d.geometry.TriangleMesh.sample_points_uniformly(mesh1, number_of_points)
		pcd1 = nornalize_unit_ball(pcd1)
		pcds_1.append(pcd1)

	return pcds_1


def generate_vidrilo_figures(pathToDataset):

	pcd_files = [y for x in os.walk(pathToDataset) for y in glob(os.path.join(x[0], '*.pcd'))]
	pcd_files = natural_sort(pcd_files)
	pcds_1 = []

	factor = None

	for model in pcd_files:
		pcd = o3d.io.read_point_cloud(model)

		if factor == None:
			factor = computeScaleFactor(pcd)
			print("The scale is",factor)

		pcd = nornalize_unit_ball(pcd, factor)
		pcds_1.append(pcd)


	return pcds_1


def scale_pcd(pcd, scale_factor):
	pcd = copy.deepcopy(pcd)
	points = np.asarray(pcd.points)
	points *= scale_factor
	pcd_scaled = o3d.geometry.PointCloud()
	pcd_scaled.points = o3d.utility.Vector3dVector(points)
	return pcd_scaled


def apply_random_T(pcd):
	pcd_r = copy.deepcopy(pcd)

	deg_x = random.randint(0,360)
	deg_y = random.randint(0,360)
	deg_z = random.randint(0,360)
	rads_x = deg_x * math.pi / 180.0
	rads_y = deg_y * math.pi / 180.0
	rads_z = deg_z * math.pi / 180.0
	R = pcd_r.get_rotation_matrix_from_xyz((rads_x, rads_y, rads_z))

	tx_1 = random.randint(0,1000)
	ty_1 = random.randint(0,1000)
	tz_1 = random.randint(0,1000)
	T = (tx_1, ty_1, tz_1)

	pcd_r = pcd_r.translate(T)
	pcd_r.rotate(R)
	return pcd_r, T, R


def distort_x_axis(pcd, factor=1.5):
	points_dist = np.asarray(pcd.points)
	points_dist[:,0] *= factor
	pcd_dist = o3d.geometry.PointCloud()
	pcd_dist.points = o3d.utility.Vector3dVector(points_dist)
	return pcd_dist


def computeScaleFactor(input_pcd):
	max_x = np.max(np.asarray(input_pcd.points)[:,0])
	min_x = np.min(np.asarray(input_pcd.points)[:,0])
	max_y = np.max(np.asarray(input_pcd.points)[:,1])
	min_y = np.min(np.asarray(input_pcd.points)[:,1])
	max_z = np.max(np.asarray(input_pcd.points)[:,2])
	min_z = np.min(np.asarray(input_pcd.points)[:,2])

	diff_x = max_x-min_x
	diff_y = max_y-min_y
	diff_z = max_z-min_z

	factor = 1
	if diff_x > diff_y and diff_x > diff_z:
		factor = 1.0/diff_x
	if diff_y > diff_x and diff_y > diff_z:
		factor = 1.0/diff_y
	if diff_z > diff_x and diff_z > diff_y:
		factor = 1.0/diff_z

	return factor


def nornalize_unit_ball(input_pcd, factor=None):

	if factor is None:
		factor = computeScaleFactor(input_pcd)

	pcd_norm = np.asarray(input_pcd.points)	
	pcd_norm = pcd_norm*factor

	min_x = np.min(pcd_norm[:,0])
	min_y = np.min(pcd_norm[:,1])
	min_z = np.min(pcd_norm[:,2])
	
	pcd_norm[:,0] -= min_x
	pcd_norm[:,1] -= min_y
	pcd_norm[:,2] -= min_z

	pcd = o3d.geometry.PointCloud()
	pcd.points = o3d.utility.Vector3dVector(pcd_norm)

	return pcd




