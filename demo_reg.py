import open3d as o3d
import numpy as np
import random
import copy
import matplotlib.pyplot as plt
from FractalDescriptor import *
from utils import *




def registration_fractal(pcd1, pcd2, radiusFactors = [3,4.5,6,7.5,9,10.5], boxCountIter = 9, distance_threshold = 1.5, bc_subdivs=2):
	ransac_n = 3

	extr = FractalDescriptorExtractor()

	pcd1_fds = extr.compute_fractal_descriptor(pcd1, radiusFactors, boxCountIter, bc_subdivs)
	pcd2_fds = extr.compute_fractal_descriptor(pcd2, radiusFactors, boxCountIter, bc_subdivs)

	pcd1_fds_filt = pcd1_fds
	pcd2_fds_filt = pcd2_fds

	pcd1_fds_ran = FractalFeature(pcd1_fds_filt)
	pcd2_fds_ran = FractalFeature(pcd2_fds_filt)
	
	result = o3d.pipelines.registration.registration_ransac_based_on_feature_matching(
		pcd1, pcd2, pcd1_fds_ran, pcd2_fds_ran, True,
		distance_threshold,
		o3d.pipelines.registration.TransformationEstimationPointToPoint(False),
		ransac_n, [
		], o3d.pipelines.registration.RANSACConvergenceCriteria(100000, 0.999))


	return pcd1_fds_ran, pcd2_fds_ran, result



def run_registration_vfld(pcd1, pcd2, radiusFactors, boxCountIter, distance_threshold, bc_subdivs):


	feat_2, feat_1, result = registration_fractal(pcd2, pcd1, radiusFactors=radiusFactors, boxCountIter=boxCountIter, distance_threshold=distance_threshold, bc_subdivs=bc_subdivs)
				

	print("\t Correspondences:",len(result.correspondence_set))
	print("\t Fitness:",result.fitness)
	print("\t Inliner RMSE:",result.inlier_rmse)


	pcd1_temp = copy.deepcopy(pcd1)
	pcd2_temp = copy.deepcopy(pcd2)
	pcd2_temp.transform(result.transformation)

	joined_cloud = pcd1_temp + pcd2_temp

	return joined_cloud, pcd2_temp
	


def main():

	# Parameters for VFLD
	s = 1
	i = 0.5
	rl = 18
	radiusFactors = np.arange(s, 999.0, i)[:rl]
	boxCountIter = 5
	bc_subdivs = 2
	distance_threshold = 3#5
	voxel_size = 2.3


	# Read dataset
	simple_figures_pcd_1 = generate_vidrilo_figures("data/vidrilo1")
	print("Number of figures:",len(simple_figures_pcd_1))


	# Perform registration
	pcd_acum = simple_figures_pcd_1[0]
	pcd_acum = scale_pcd(pcd_acum,100)
	pcd_acum = pcd_acum.voxel_down_sample(voxel_size=voxel_size)
	pcd_acum.paint_uniform_color([random.random(), random.random(), random.random()])
	pcd_ant = simple_figures_pcd_1[0]
	pcd_ant = scale_pcd(pcd_ant,100)
	pcd_ant = pcd_ant.voxel_down_sample(voxel_size=voxel_size)
	pcd_ant.paint_uniform_color([random.random(), random.random(), random.random()])

	for i in range(1, len(simple_figures_pcd_1)):
		print("Registering cloud idx",i+1,"/",len(simple_figures_pcd_1))
		pcd_2 = simple_figures_pcd_1[i]
		pcd_2 = scale_pcd(pcd_2,100)
		pcd2_down = pcd_2.voxel_down_sample(voxel_size=voxel_size)
		pcd2_down.paint_uniform_color([random.random(), random.random(), random.random()])

		print(pcd2_down)

	
		# VFLD registration pipeline
		_, pcd2_down_transformed = run_registration_vfld(pcd_ant, pcd2_down, radiusFactors, boxCountIter, distance_threshold, bc_subdivs)


		pcd_acum += pcd2_down_transformed
		pcd_ant = pcd2_down_transformed


		print("This is the registration so far, press 'q' to continue")
		pcd_acum_temp = pcd_acum.voxel_down_sample(voxel_size=voxel_size)
		o3d.visualization.draw_geometries([pcd_acum_temp])





	

if __name__ == '__main__':
	main()